# Wallpaper Repository

This repository contains the wallpapers that I have used or I find interesting.

## Sources :

  1. Some of the wallpapers are taken from the [repository](https://gitlab.com/dwt1/wallpapers) of Derek Taylor (DistroTube).
  
  2. Rest are taken from the some websites freely available in the internet.



